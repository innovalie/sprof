# -*- coding: utf-8 -*
# python3
# Project local settings
from os.path import dirname, realpath, join

# project dir - do not update
PROJECT_DIR = dirname(dirname(realpath(__file__)))

# Default settings values

# Default directory to search radar datas
RADAR_DATA_DIR = join(PROJECT_DIR,'test')

# File name and directory containing athletes data
ATHLETE_DATA_DIR = join(PROJECT_DIR,'data')
ATHLETE_DATA_FILE = "athlete_data.csv"

PFV_ANALYSE_DIR = join(PROJECT_DIR,'test')

EXPORT_CSV_DECIMAL='.'
EXPORT_CSV_SEPARATOR=';'
CSV_ATHLETE_SEPARATOR=';'

# if debug = true, show more messages on execution.
# Partially used
DEBUG = False

# List of exported column in the csv files
EXPORT_COLS=['name','mass','V0', 'F0','F0_kg','Pmax','Pmax_kg','sfv','RF_peak', 'DRF','top_speed','tau' ]

# List of exported time and distance values
# List of times to reach the following distances :
EXPORT_TIMES=(5,10,20,30)
# list of distances reached after the following times:
EXPORT_DISTANCES=(2,4)
''' Exemple
-- Performances during acceleration
    Time @ 5m	1.34 s
    Time @ 10m	2.06 s
    Time @ 20m	3.32 s
    Time @ 25m	3.91 s
    Time @ 30m	4.50 s
    Distance in 2s	9.50 m
    Distance in 4s	25.71 m
'''
# local values overwrites default values
#from sprof.settings_local import *
try:
    from sprof.settings_local import *
except ImportError:
    pass

'''
def get_logger():
    mpl_logger = logging.getLogger('matplotlib')
    mpl_logger.setLevel(logging.WARNING)

    logging.basicConfig(format='%(levelname)s : %(name)s : %(message)s',level=logging.DEBUG)
    logger = logging.getLogger('sprof')
    return logger

    #logger.info('This is an info message from sprof app')
    #logging.info('This is an info message from sprof app')
    #logging.debug('This is a debug message from sprof app')
'''
