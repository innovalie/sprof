# sprof - Installation et utilisation sous Windows


## Installation de python 3

Note 1 : il faut une connexion internet pour cette étape

note 2 : pour vérifier si on est en 32 ou 64 : taper `système` dans la recherche pour ouvrir le panneau de configuration, et regarder dans `Système\type du système`

### 1) Vérifier si python est est déjà installé

- Taper `dos` dans la recherche, et ouvrir l'application `invite de commande``
- Tester la commande `python` ou éventuellement `python3`
- Tester aussi la commande `git`, tant qu'à faire

### 2) Installation python

Site de téléchargement : https://www.python.org/downloads/windows/, ou faire une recherche depuis la barre de recherche windows.

Choisir le lien pour python 3.9, de type : `Download Windows x86-64 executable installer`

Cliquer sur l'exécutable téléchargé. (Par défaut il est dans home\Downloads)

**Attention :** Bien cocher l'option de mise à jour du path, pour que la commande `python` soit accessible de tous les répertoires (add python to env variables)

Il faut aussi que pip soit disponible, mais il l'est par défaut.

test : la commande python ou python3 depuis le dos doit marcher.

Mise à jour 2023 : installation sans droit admin ok. Il faut choisir l'installation qui donne accès aux options, pas celle par défaut, et décocher l'option 'install for all users'

## Installation de git

Facultatif, mais facilite grandement les mises à jour potentielles du code.

Il y a plusieurs manière d'installer git sous indows, comme décrit dans la page https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Installation-de-Git

Exemple depuis https://git-scm.com/download/win, en utilisant winget :

```
# Depuis l'invite de commande dos
winget install --id Git.Git -e --source winget
# Note : Réouvrir dos pour que la cde git soit disponible
```

## Installation de sprof

### Installer

1) Créer un dossier pour le projet (par exemple, `radar`)

2) Récupérer le code source

- Avec git, depuis le répertoire projet : `git clone https://gricad-gitlab.univ-grenoble-alpes.fr/innovalie/sprof.git`

- Sans git, télécharger le zip des sources depuis la page du projet (https://gricad-gitlab.univ-grenoble-alpes.fr/innovalie/sprof) en cliquant sur le bouton de téléchargement. Le lien direct est https://gricad-gitlab.univ-grenoble-alpes.fr/innovalie/sprof/-/archive/master/sprof-master.zip

Bien garder le nom sprof, si besoin renommer sprof-master

3) Ouvrir l'invite de commande et taper les commandes :
```console
# Se placer dans le répertoire du projet
cd radar
# création et activation environnement virtuel python :
python -m venv .\venv_sprof
venv_sprof\Scripts\activate.bat
# virtualenv -p python3 venv_sprof # anciennes versions
# Installation paquets python du projet :
cd ..\sprof
pip install -r requirements.txt
pip install -e .
```

### Viewer csv

Pour l'utilisation des exécutables windows `sprof_watcher.bat` et `sprof_scandir.bat`, il faut utiliser un viewer csv. Excel est possible (dans ce cas, mettre à jour les .bat), mais il existe des viewer plus simples, avec un refresh automatique possible.

La solution testée pour sprof est le viewer csv de nirsoft, à télécharger ici : https://www.nirsoft.net/utils/csvfileview-x64.zip

Télécharger l'utilitaire et le dézipper dans le répertoire `.../sprof/win`

En ligne de commande :
```
cd .../sprof/win
mkdir csvfileview-x64
cd csvfileview-x64
curl.exe --output csvfileview-x64.zip --url https://www.nirsoft.net/utils/csvfileview-x64.zip
tar -xf csvfileview-x64.zip
```

Pour lancer le viewer, cliquer sur le fichier csvfileview-x64/CSVFileView.exe

Activer le mode auto-refresh dans l'outil csvfileview. Ce n'est pas le mode par défaut.(Options/Auto Refresh ; puis Options/Auto Refresh Mode/Full)

### Tester

Pour vérifier que le code tourne correctement sur les données de test: (La première execution prend beaucoup de temps, c'est normal il faut créer les fichiers .pyc)
```console
cd sprof # se placer dans le répertoire project_dir/sprof/sprof
python radar_data.py -p oden1
python sprint.py
python pfv_dataset.py
python analyse.py
```

### Configurer

On peut configurer :
- Le répertoire par défaut dans lequel sprof ira chercher les données radar,
- Le fichier dans lequel se trouvent la masse et la stature des athletes,
- Les fichiers d'export csv - emplacement, caractère séparateur et décimale.
- La liste des colonnes à exporter dans les fichiers csv

Pour cela, il faut copier le fichier `sprof\sprof\settings_local_sample.py` et le renommer en `settings_local.py`
```console
copy settings_local_sample.py settings_local.py
```

Puis ouvrir `settings_local.py` et y apporter les modifications nécessaires.

Faire de même avec le fichier des athletes :
```console
copy data/athlete_data_sample.csv data/athlete_data.csv
```

Conseil, si on n'a pas récupéré le code avec git clone : sauvegarder une version de la configuration locale hors du code source, car en cas de mise à jour il risque d'être effacé, en utilisant par ex un répertoire 'local' :
```
mkdir ..\..\local
copy settings_local.py ..\..\local\
```
De même, si on n'utilise par git, ne pas mettre dans le répertoire du code source les fichiers de données des athletes. Les mettre par exemple dans ce répertoire 'local'

pour tester que le répertoire des données radar par défaut est valide :
```console
python radar_file.py # à partir de `...\sprof\sprof`
```

pour tester que le fichier contenant les données des athletes est bien trouvé :
```console
python athlete.py # à partir de `...\sprof\sprof`
```

## Utilisation

### Executables

Des exécutables dans le répertoire `\sprof\win\` permettent de lancer les principales fonctionnalités.

- `sprof_analyse` : boucle sur un répertoire (par défault, ou selectionné) pour lancer un à un l'analyse pfv des fichiers souhaités
- `sprof_scandir` : scanne un répertoire de données choisi et génère un fichier csv avec lers réstultats des analyses pfv. Ce fichiers est sauvé dans le répertoire des données, et dans un répertoire d'analyse
- `sprof_watcher` : surveille un répertoire, et analyse tout nouveau fichier de données radar qui y est placé. Génère un fichier csv des résultats dans ce répertoire et dans le répertoire d'analyse
- `sprof_movebounds` : permet de tester les variations des parametres PFV pour un fichier de donnees lorsque l'on fait varier les limites du sprint
- `sprof_cdes` : Permet d'utiliser directement les commandes python sprof (= Active le virtualenv python, et de place dans le répertoire sprof du projet).

sprof_watcher et sprof_scandir ouvrent le fichier de résultat csv avec l'utilitiare `csv file viewer` de nirsoft. Pour installer l'utilitaire, voir la partie Installation de ce document.

Penser à activer le mode auto-refresh dans l'outil csvfileview. Ce n'est pas le mode par défaut.(Options/Auto Refresh ; puis Options/Auto Refresh Mode/Full)

On peut aussi aller modifier les exécutables pour utiliser plutôt excel, ou open office (ou rien).

### Sous dos
- Ouvrir une invite de commande dos
- activer le virtual env python si ce n'est pas déjà fait :
`....\venv_sprof\Scripts\activate.bat`
- Se placer dans le répertoire `...\sprof\sprof`

Test avec PowerShell (windows10): la commande activate.ps1 ne fonctionne pas

### Exemples

Voir le README

## Mise à jour du code source sprof

**Si on a git**, il suffit de faire un git pull
```
cd ...\sprof
git pull
```
Si des nouvelles variables d'environnement sont définies dans settings_local_sample.py, on peut si besoin les copier dans settings_local.py et les modifier.

**Si on n'a pas git**

Il faut recopier les fichiers sources en remplaçant les précédants, mais attention à ne pas effacer le fichier de conf local `settings_local.py`, ou alors penser à le remettre à partir d'un emplacement où il a été sauvegardé ('local' par ex). Pareil pour le fichier des données athletes si besoin.

**Si on veut changer le code de place**, il faut désinstaller le précédent :

```console
pip uninstall sprof
````

Note (ancien) :

Sous windows il faut aussi éditer le fichier `.../venv_sprof/Lib/site-packages/easy-install.pth` et effacer la ligne qui n'est plus valide. Puis on peut ré-installer sprof, à partir du répertoire racine sprof : `python setup.py develop`

Mise à jour mars 2021 : avec l'utilisation de `pip install -e .`, à priori plus besoin de faire cette manip


## Utile

Quelques commandes sous dos, l'invite de commande windows :

- cd : change de répertoire
- dir : liste le contenu d'un répertoire
- mkdir : créé un répertoire
- del : delete file
- copy : copie un fichier vers un autre
- move : déplace un fichier
- rmdir /s/q nom_repertoire : équivalent rm -rf linux

Memo sur mon poste maison windows:
Les fichiers sont dans C(Acer):Utilisateur puis Caro. C'est le répertoire
équivalent à home : si on lance l'invite de commande on est dans C:\Users\Caro>
