:: lancer le data watcher sprof
@echo on
:: On se déplace dans le répertoire projet sprof
cd /d "%~dp0..\sprof"
:: active le virtualenv
call ..\..\venv_sprof\Scripts\activate.bat
::  et on laisse ouvert l'invite de commandes
cmd /k
